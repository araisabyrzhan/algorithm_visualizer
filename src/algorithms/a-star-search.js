import {getAllNodes, getUnvisitedNeighbors} from './graph';

export function aStarSearch(grid, startNode, endNode, heuristic) {
    const visitedNodesInOrder = [];
    startNode.distance = 0;
    startNode.totalDistance = 0;

    const unvisitedNodes = getAllNodes(grid);

    while (!!unvisitedNodes.length) {
        sortNodesByDistance(unvisitedNodes);
        let closestNode = unvisitedNodes.shift();
        while (closestNode.isWall && unvisitedNodes.length) {
            sortNodesByDistance(unvisitedNodes);
            closestNode = unvisitedNodes.shift();
        }
        
        if (closestNode.isWall) continue;

        if (closestNode.distance === Infinity) return visitedNodesInOrder;
        closestNode.isVisited = true;
        visitedNodesInOrder.push(closestNode);
        if (closestNode === endNode) return visitedNodesInOrder;
        updateUnvisitedNeighbors(closestNode, endNode, grid);
    }
};

function sortNodesByDistance(unvisitedNodes) {
    unvisitedNodes.sort(function(nodeA, nodeB) {
        return nodeA.totalDistance - nodeB.totalDistance || nodeA.heuristicDistance - nodeB.heuristicDistance;
    });
}

function updateUnvisitedNeighbors(node, endNode, grid) {
    const unvisitedNeighbors = getUnvisitedNeighbors(node, grid);
    for (const neighbor of unvisitedNeighbors) {
        neighbor.heuristicDistance = manhattanDistance(neighbor, endNode);
        if (node.distance + 1 < neighbor.distance) {
            neighbor.distance = node.distance + 1;
            neighbor.totalDistance = neighbor.distance + neighbor.heuristicDistance;
            neighbor.previousNode = node;
        }
        
    }
  }

function manhattanDistance(node, endNode) {
    const colChange = Math.abs(node.col - endNode.col);
    const rowChange = Math.abs(node.row - endNode.row);
    return (colChange + rowChange);
}