import React, {Component} from 'react';

class NavbarComponent extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <>
            <form>
                <div className="realestate-filter">
                    <div className="container">
                    <div className="realestate-filter-wrap nav">
                        {/* eslint-disable-next-line */}
                        <a href="#for-rent" className="active" data-toggle="tab" id="rent-tab" aria-controls="rent" aria-selected="true">Path finding</a>
                        {/* <a href="#for-sale" className="" data-toggle="tab" id="sale-tab" aria-controls="sale" aria-selected="false">Sorting</a> */}
                    </div>
                    </div>
                </div>
              
              <div className="realestate-tabpane pb-5">
                <div className="container tab-content">
                <div className="tab-pane active" id="for-rent" role="tabpanel" aria-labelledby="rent-tab">
                <div className="row">
                    <div className="col-md-4 form-group">
                        <select name="Algorithm" id="" className="form-control w-100" onChange={this.props.handleAlgorithmChange}>
                            <option value="" selected disabled>Algorithms</option>
                            <option value="dfs">DFS</option>
                            <option value="bfs">BFS</option>
                            <option value="dijkstra">Dijkstra</option>
                            <option value="a-star">A *</option>
                            <option value="greedy-bfs">Greedy Best-first Search</option>
                         </select>
                    </div>
                    <div className="col-md-4 form-group">
                        <select name="" id="" className="form-control w-100" onChange={this.props.handleMazeChange}>
                            <option value="" selected disabled>Maze</option>
                            <option value="random">Random maze</option>
                        </select>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-4 form-group">
                        <input type="button" className="btn btn-black py-3 btn-block" value="Start search" onClick={this.props.handleStartSearch} disabled={this.props.startSearch}/>
                    </div>
                    <div className="col-md-4 form-group">
                        <input type="button" className="btn btn-black py-3 btn-block" value="Clear board" onClick={this.props.handleClearBoard} disabled={this.props.startSearch}/>
                    </div>
                </div>
        
                {/* <div className="row">
                    <div className="col-md-4 form-group">
                        <select name="" id="" className="form-control w-100">
                            <option value="" selected disabled>Speed</option>
                            <option value="">0</option>
                            <option value="">0</option>
                            <option value="">0</option>
                        </select>
                    </div>
                    <div className="col-md-4 form-group">
                        <input type="button" className="btn btn-black py-3 btn-block" value="Start search" onClick={this.handleStartSearch}/>
                    </div>
                    <div className="col-md-4 form-group">
                        <input type="button" className="btn btn-black py-3 btn-block" value="Clear" onClick={this.handleClearBoard}/>
                    </div>
                </div> */}
        
            </div>
                </div>
    
              </div>
            </form>
            </>
        );
    }

}

export  default NavbarComponent;