import React, {Component} from 'react';
import Node from './node';
import { aStarSearch, 
    breadthFirstSearch, 
    depthFirstSearch, 
    dijkstra, 
    getNodesInShortestPathOrder,
    greedyBestFirstSearch,
    randomMaze,
} from '../algorithms';

const START_NODE_ROW = 10;
const START_NODE_COL = 10; // 10/25
const END_NODE_ROW = 10;
const END_NODE_COL = 45; // 35/50

export default class PathfindingVisualizerComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            grid: [],
            mouseIsPressed: false,
        };
    }

    componentDidMount() {
        const grid = getInitialGrid();
        this.setState({grid});
    }

    componentDidUpdate() {
        if(this.props.mazeName !== '') {
            this.generateMaze();
        }
        if (this.props.isClearPath) {
            const clearingBoard = this.props.isClearBoard;
            this.clearingPath(clearingBoard);
        }        
    }

    handleMouseDown(row, col) {
        const newGrid = getNewGridWithWallToggled(this.state.grid, row, col);
        this.setState({grid: newGrid, mouseIsPressed: true});
    }

    handleMouseEnter(row, col) {
        if(!this.state.mouseIsPressed) return; 
        const newGrid = getNewGridWithWallToggled(this.state.grid, row, col);
        this.setState({grid: newGrid});
    }

    handleMouseUp() {
        this.setState({mouseIsPressed: false});
    }

    clearingPath(clearingBoard) {
        const grid = this.state.grid;

        for (let i = 0; i < grid.length; i++) {
            for(let j = 0; j < grid[i].length; j++) {
                const node = grid[i][j];
                node.isVisited = false;
                node.distance = Infinity;
                node.heuristicDistance = null;
                node.totalDistance = Infinity;
                node.previousNode = null;
                if (node.isStart) {
                    document.getElementById(`node-${node.row}-${node.col}`).className = 'node node-start';
                } else if (node.isEnd) {
                    document.getElementById(`node-${node.row}-${node.col}`).className = 'node node-end';
                }

                if (!node.isStart && !node.isEnd && !node.isWall && !clearingBoard) {
                    document.getElementById(`node-${node.row}-${node.col}`).className = 'node';
                } else if (!node.isStart && !node.isEnd && clearingBoard) {
                    node.isWall = false;
                    document.getElementById(`node-${node.row}-${node.col}`).className = 'node';
                }
            }
        }
        if (clearingBoard) this.props.isClearEnd();
    }

    generateMaze() {
        const {grid} = this.state;
        const clearingBoard = true;
        
        this.clearingPath(clearingBoard);
        randomMaze(grid);
        this.props.isMazeGenerateEnd();
    }

    animateAlgorithm(visitedNodesInOrder, shortestNodesInOrder) {
        for (let i = 0; i <= visitedNodesInOrder.length; i++) {
            if (i === visitedNodesInOrder.length) {
                setTimeout(() => {
                    this.animateShortestPath(shortestNodesInOrder);
                }, 10 * i);
                return;
            }
            setTimeout(() => {
                const node = visitedNodesInOrder[i];
                document.getElementById(`node-${node.row}-${node.col}`).className = 'node node-visited';
            }, 10 * i);
        }
    }

    animateShortestPath(shortestNodesInOrder) {
        for (let i = 0; i < shortestNodesInOrder.length; i++) {
            setTimeout(() => {
                const node = shortestNodesInOrder[i];
                document.getElementById(`node-${node.row}-${node.col}`).className = 'node node-shortest-path';
            }, 50 * i);
        }
        this.props.isSearchEnd();
    }

    visualize(algorithm) {
        const {grid} = this.state;
        const startNode = grid[START_NODE_ROW][START_NODE_COL];
        const endNode = grid[END_NODE_ROW][END_NODE_COL];
        let heuristicDistance = null;
        let visitedNodesInOrder = [];
        switch (algorithm) {
            case 'dfs':
                visitedNodesInOrder = depthFirstSearch(grid, startNode);
                break;
            case 'bfs':
                visitedNodesInOrder = breadthFirstSearch(grid, startNode);
                break;
            case 'dijkstra':
                visitedNodesInOrder = dijkstra(grid, startNode, endNode);
                break;
            case 'a-star':
                visitedNodesInOrder = aStarSearch(grid, startNode, endNode, heuristicDistance);
                break;
            case 'greedy-bfs':
                visitedNodesInOrder = greedyBestFirstSearch(grid, startNode, endNode, heuristicDistance);
                break;
            default:
                visitedNodesInOrder = [];
                console.log('Algorithm is incorrect');
                break;
        }
        const shortestNodesInOrder = getNodesInShortestPathOrder(endNode);
        this.animateAlgorithm(visitedNodesInOrder, shortestNodesInOrder);
    }

    render () {
        const { grid, mouseIsPressed }  = this.state;
        const {algorithmName, startSearch} = this.props;
        if (startSearch) {
            this.visualize(algorithmName);
        }
        
        return (
            <table>
                {grid.map((row, rowIndex) => {
                    return (
                        <tr key={rowIndex}>
                            {row.map((node, nodeIndex) => {
                                const {row, col, isEnd, isStart, isWall} = node;
                                return (
                                    <Node
                                        key={nodeIndex}
                                        col={col}
                                        isEnd={isEnd}
                                        isStart={isStart}
                                        isWall={isWall}
                                        mouseIsPressed={mouseIsPressed}
                                        onMouseDown={(row, col) => this.handleMouseDown(row, col)}
                                        onMouseEnter={(row, col) => this.handleMouseEnter(row, col)}
                                        onMouseUp={() => this.handleMouseUp()}
                                        row={row}
                                    ></Node>
                                );
                            })}
                        </tr>
                    );
                })}
            </table>
        );
    }
}

const getInitialGrid = () => {
    const grid = [];
    for (let row = 0; row < 28; row++) { 
        const currentRow = [];
        for (let col = 0; col < 60; col++) { // 50/70
            currentRow.push(createNode(col, row));
        }
        grid.push(currentRow);
    }
    return grid;
};

const createNode = (col, row) => {
    return {
        col, row,
        isStart: row === START_NODE_ROW && col === START_NODE_COL,
        isEnd: row === END_NODE_ROW && col === END_NODE_COL,
        distance: Infinity,
        isVisited: false,
        isWall: false,
        previousNode: null,

        totalDistance: Infinity,    // A-star parameter
        heuristicDistance: null,    // A-star parameter
    };
};

const getNewGridWithWallToggled = (grid, row, col) => {
    const newGrid = grid.slice();
    const node = newGrid[row][col];
    const newNode = {
        ...node,
        isWall: !node.isWall,
    };
    newGrid[row][col] = newNode;
    return newGrid;
}