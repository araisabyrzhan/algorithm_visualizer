import React from 'react';
import NavbarComponent from './assemble/navbar';
import PathfindingVisualizerComponent from './assemble/pathfindingVisualizer';

import './styles/app.css';

class App extends React.Component {
    constructor() {
        super();
        this.state = {
            algorithmName: '',
            mazeName: '',
            startSearch: false,
            isClearBoard: false,
            isClearPath: false,
        };
    }

    handleAlgorithmChange = (event) => {
        this.setState({
            algorithmName: event.target.value, 
            startSearch: false, 
            isClearPath: true, 
            isClearBoard: false
        });
    }

    handleMazeChange = (event) => {
        this.setState({
            mazeName: event.target.value,
        });
    }

    handleStartSearch = () => {
        this.setState({
            startSearch: true, 
            isClearPath: false, 
            isClearBoard: false
        });
    }

    handleClearBoard = () => {
        this.setState({
            mazeName: '',
            startSearch: false,
            isClearPath: true,
            isClearBoard: true
        });
    }
    
    searchEnd = () => {
        this.setState({startSearch: false});
    }

    clearEnd = () => {
        this.setState({
            isClearPath: false,
            isClearBoard: false,
        });
    }

    mazeGenerateEnd = () => {
        this.setState({mazeName: ''});
    }

    render() {
        return (
            <div className="App">
                <div className="grid">
                    <PathfindingVisualizerComponent 
                        algorithmName={this.state.algorithmName} 
                        mazeName={this.state.mazeName}
                        startSearch={this.state.startSearch}
                        isClearBoard={this.state.isClearBoard}
                        isClearPath={this.state.isClearPath}
                        isClearEnd={this.clearEnd}
                        isSearchEnd={this.searchEnd}
                        isMazeGenerateEnd={this.mazeGenerateEnd}
                    ></PathfindingVisualizerComponent>
                    
                    <NavbarComponent
                        startSearch={this.state.startSearch}
                        handleAlgorithmChange={this.handleAlgorithmChange}
                        handleMazeChange={this.handleMazeChange}
                        handleStartSearch={this.handleStartSearch}
                        handleClearBoard={this.handleClearBoard}
                    >
                    </NavbarComponent>
                </div>
            </div> 
        );
    }
}

export default App;