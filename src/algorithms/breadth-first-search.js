export function breadthFirstSearch(grid, startNode) {
    const visitedNodesInOrder = [];
    startNode.distance = 0;
    startNode.isVisited = true;
    visitedNodesInOrder.push(startNode);
    return breadthFirstSearchImplementation(grid, startNode, visitedNodesInOrder);
};

function breadthFirstSearchImplementation(grid, startNode, visitedNodesInOrder) {
    let queue = [startNode];
    while (!!queue.length) {
        let node = queue.shift();
        if (node.isEnd) return visitedNodesInOrder;

        let unvisitedNeighbors = getUnvisitedNeighbors(node, grid);
        while (!!unvisitedNeighbors.length) {
            let closestNode = unvisitedNeighbors.shift();

            if(closestNode.isWall) continue;

            closestNode.isVisited = true;
            visitedNodesInOrder.push(closestNode);
            updateUnvisitedNeighbor(closestNode, node, grid);
            queue.push(closestNode);

            if(closestNode.isEnd) return visitedNodesInOrder;
        }
    }
}
function updateUnvisitedNeighbor(neighbor, node, grid) {
    const {col, row} = neighbor;
    grid[row][col].distance = node.distance + 1;
    grid[row][col].previousNode = node;
}

function getUnvisitedNeighbors(node, grid) {
    const neighbors = [];
    const {col, row} = node;
    if (row > 0) neighbors.push(grid[row - 1][col]);
    if (row < grid.length - 1) neighbors.push(grid[row + 1][col]);
    if (col > 0) neighbors.push(grid[row][col - 1]);
    if (col < grid[0].length - 1) neighbors.push(grid[row][col + 1]);
    return neighbors.filter(neighbor => !neighbor.isVisited);
  }