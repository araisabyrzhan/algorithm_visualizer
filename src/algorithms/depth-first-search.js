export function depthFirstSearch(grid, startNode) {
    const visitedNodesInOrder = [startNode];
    startNode.distance = 0;
    return depthFirstSearchImplementation(grid, startNode, visitedNodesInOrder);
}

function depthFirstSearchImplementation(grid, node, visitedNodesInOrder) {
    if (node.isVisited) return false;

    node.isVisited = true;
    visitedNodesInOrder.push(node);

    if (node.isEnd === true) return visitedNodesInOrder;

    const {row, col} = node;
    let right = col + 1, left = col - 1, up = row - 1, down = row + 1;

    while (up >= 0 && !grid[up][col].isVisited && !grid[up][col].isWall) { // up
        updateVisitedNeighbor(node, grid[up][col], grid);
        if (depthFirstSearchImplementation(grid, grid[up][col], visitedNodesInOrder)) return visitedNodesInOrder;
    }

    while (right < grid[0].length && !grid[up + 1][right].isVisited && !grid[up + 1][right].isWall) { // right
        updateVisitedNeighbor(node, grid[up + 1][right], grid);
        if (depthFirstSearchImplementation(grid, grid[up + 1][right], visitedNodesInOrder)) return visitedNodesInOrder;
    } 

    while (down < grid.length && !grid[down][right - 1].isVisited && !grid[down][right - 1].isWall) { // down
        updateVisitedNeighbor(node, grid[down][right - 1], grid);
        if (depthFirstSearchImplementation(grid, grid[down][right - 1], visitedNodesInOrder)) return visitedNodesInOrder;
    }

    while (left >= 0 && !grid[down - 1][left].isVisited && !grid[down - 1][left].isWall) { // left
        updateVisitedNeighbor(node, grid[down - 1][left], grid);
        if (depthFirstSearchImplementation(grid, grid[down - 1][left], visitedNodesInOrder)) return visitedNodesInOrder;
    }
    return false;
}

function updateVisitedNeighbor(node, neighbor, grid) {
    const {col, row} = neighbor;
    grid[row][col].distance = node.distance + 1;
    grid[row][col].previousNode = node;
}