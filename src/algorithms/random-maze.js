import {getAllNodes} from './graph';

export function randomMaze(grid, startNode, endNode) {
    const allNodes = getAllNodes(grid);

    for (const node of allNodes) {
        let random = Math.random();
        
        if (random < 0.2 && !node.isStart && !node.isEnd) {
            node.isWall = true;
            document.getElementById(`node-${node.row}-${node.col}`).className = 'node node-wall';
        }
    }
    
}